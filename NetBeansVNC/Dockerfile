FROM java:8-jdk

ENV DEBIAN_FRONTEND noninteractive 

RUN apt-get update 
RUN apt-get install -y xfce4 \
	xvfb \
	x11vnc \
	supervisor

RUN adduser --disabled-password --gecos '' johan

ADD vnc.conf /etc/supervisor/conf.d/

WORKDIR /tmp

# Install Netbeans
RUN wget http://download.netbeans.org/netbeans/8.0.2/final/bundles/netbeans-8.0.2-linux.sh
RUN chmod +x netbeans*.sh
RUN sh netbeans*.sh --silent

# Install Eclipse
RUN wget http://ftp.fau.de/eclipse/technology/epp/downloads/release/mars/R/eclipse-jee-mars-R-linux-gtk-x86_64.tar.gz
RUN tar -xzvf eclipse*.tar.gz -C /opt

# Install IntelliJ
RUN wget http://download.jetbrains.com/idea/ideaIC-14.1.5.tar.gz
RUN mkdir -p /opt/intellij
RUN tar -xf idea*.tar.gz --strip-components=1 -C /opt/intellij

# Cleanup
RUN rm -rf /tmp/*

# Create links
RUN echo "/opt/eclipse/eclipse" > /tmp/eclipse.sh
RUN echo "/opt/intellij/bin/idea.sh" > /tmp/intellij.sh
RUN chmod +x /tmp/*.sh

# Define folders for Netbeans
RUN sed -i 's/^netbeans_default_userdir.*/netbeans_default_userdir=\/workspace\/userdir/' /usr/local/netbeans-8.0.2/etc/netbeans.conf
RUN sed -i 's/^netbeans_default_cachedir.*/netbeans_default_cachedir=\/workspace\/cachedir/' /usr/local/netbeans-8.0.2/etc/netbeans.conf

EXPOSE 5900
CMD ["supervisord", "-n"]
